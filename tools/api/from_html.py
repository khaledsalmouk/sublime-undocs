import os
import re
# import textwrap

import bs4

from operator import xor

from st_api import (SublimeApi, Module, Class, Function, Method, Property,
                    Constructor)


def custom_dedent(string):
    """Works like textwraps.dedent but bases the indentation on the first
    indented line and not the first line.

    Also removes trailing spaces.
    """
    indent = ''
    lines = string.splitlines()

    # Search for first indent
    for l in lines:
        m = re.search(r'^[ \t]+', l)
        if m:
            indent = m.group(0)
            break

    # Then strip it
    for i, l in enumerate(lines):
        if l.startswith(indent):
            lines[i] = l[len(indent):]
        lines[i] = lines[i].rstrip()  # and remove trailing spaces

    return '\n'.join(lines).strip()


def remove_trailing_spaces(string):
    return '\n'.join(string.splitlines().rstrip())


def postprocess_html(text):
    """Apply some post-processing to the html string:

    - replacing html tags with .rst equivalents
    - replace closing tags that are generated by bs4
    """
    text = re.sub(r'</?tt>', '`', text)
    text = text.replace(r'<li>', '- ')
    text = re.sub(r'</?(ul|p|li)>', '', text)
    # text = re.sub(r'</\w+>', '', text)
    return text


def guess_type(string, just_guess=True):
    if (string.endswith("name") or string.endswith("path")
            or string in ("string", "key")):
        return "str"
    elif string == "edit":
        return "Edit"
    elif string.startswith("region"):
        return "Region"
    elif string == "view":
        return "View"
    elif string == "window":
        return "Window"
    elif string in ("point", "index", "flags", "group"):
        return "int"
    elif string.startswith("on_"):
        return "callable"
    elif string in ("String", "Int"):
        return string.lower()
    elif string == "Dictionary":
        return string.lower()
    elif string.lower() == "vector":
        return "2-tuple"
    elif just_guess:
        return ''
    else:
        return string


def parse_parameters(func):
    # Extract function name and derive signature
    m = re.match(r'^(\w+)\((.*)\)$', func)
    assert m, "Could not match: " + func
    func_name, parameter_str = m.groups()

    parameters = []
    if parameter_str:
        for param in parameter_str.split(','):
            optional = False
            param = param.strip()
            assert param

            # <param> is optional and [string] means a list type is
            # expected
            is_list = False
            if param[0] == '<':
                # One parameter is transcripted as "<animate<"
                assert param[-1] in ('>', '<'), ("%s-%s"
                                                 % (func_name, param))
                param = param[1:-1]
                optional = True
            elif param[0] == '[':
                assert param[-1] == ']', "%s-%s" % (func_name, param)
                param = param[1:-1]
                is_list = True

            # Do some type guessing
            p_type = guess_type(param)
            if is_list:
                p_type = "list(%s)" % p_type

            parameters.append(dict(name=param, type=p_type,
                                   optional=optional))
    return func_name, parameters


class FromHtmlReader(object):
    """Translates the Sublime Text from .html representation into `SublimeApi`.
    """

    def __init__(self, html):
        self.html = html

    def extract_table_rows(self, table, length):
        """Return a list of length-sized list of all rows' contents"""
        rows = []
        for tr in table.find_all('tr')[1:]:
            data = []
            for item in tr.find_all('td'):
                text = ''.join(map(str, item.contents))
                # Replace some html tags with .rst equivalents
                text = postprocess_html(text)
                # Fix indentation and trailing sapces
                text = custom_dedent(text)
                data.append(text)

            assert len(data) == length, "Found more cells than expected"

            # Make constructors/function signatures one-lined
            data[0] = ' '.join(data[0].split())

            rows.append(data)

        return rows

    def extract_constructors(self, table):
        assert table.attrs['class'] == ['functions']
        assert table.th.string == 'Constructors'
        rows = self.extract_table_rows(table, 2)
        constrs = []
        for func, desc in rows:
            _, parameters = parse_parameters(func)
            constrs.append(Constructor(parameters, desc))
        return constrs

    def extract_properties(self, table):
        assert table.attrs['class'] == ['functions']
        assert table.th.string == 'Properties'
        rows = self.extract_table_rows(table, 3)
        return [Property(*row) for row in rows]

    def extract_functions(self, T, table):
        assert table.attrs['class'] == ['functions']
        assert table.th.string == 'Methods'
        rows = self.extract_table_rows(table, 3)
        funcs = []
        for func, ret_type, desc in rows:
            if func == "(no methods)":
                continue

            func_name, parameters = parse_parameters(func)

            # Correct return type
            # [string] means a list type is expected
            is_list = False
            if ret_type[0] == '[':
                assert ret_type[-1] == ']', "%s-%s" % (func_name, ret_type)
                ret_type = ret_type[1:-1]
                is_list = True

            ret_type = guess_type(ret_type, False)
            if is_list:
                ret_type = "list(%s)" % ret_type

            # Finally append the function
            funcs.append(T(func_name, parameters, ret_type, desc))

        return funcs

    def make_class(self, name):
        description = ''
        constructors = properties = methods = None

        while True:
            child = next(self.children, None)
            if not child or child.name == 'a' and child.get('name'):
                if child:
                    # Beginning of a new class or module
                    module_name, _, class_name = (child.get('name')
                                                  .partition('.'))
                else:
                    # End of document
                    module_name = class_name = None
                # Some post-processing for the description
                description = custom_dedent(postprocess_html(description))
                return (Class(name, constructors, properties, methods,
                              description),
                        module_name,
                        class_name)

            elif child.name == 'table':
                table_type = child.th.text
                if table_type == 'Constructors':
                    constructors = self.extract_constructors(child)
                    print("Found", len(constructors), "constructors for module",
                          name)
                elif table_type == 'Properties':
                    properties = self.extract_properties(child)
                    print("Found", len(properties), "properties for module",
                          name)
                elif table_type == 'Methods':
                    methods = self.extract_functions(Method, child)
                    print("Found", len(methods), "methods for module", name)
                else:
                    assert False, "Unrecognized table type " + table_type

            else:
                # We didn't do anything with the element, so we just add it to
                # the description
                description += str(child)

    def make_module(self, name):
        # Collect these values
        description = ''
        functions = None
        classes = []
        # Indicator of next class name
        class_name = None

        child = None
        while True:
            if class_name:
                st_cls, module_name, class_name = self.make_class(class_name)
                classes.append(st_cls)
                print("created class", st_cls.name)
                assert xor(module_name == name, not class_name)
                if not class_name:
                    print("Found", len(classes), "classes for module", name)
                    return Module(name, functions, classes), module_name
                continue

            child = next(self.children, None)
            if not child:
                print("Found", len(classes), "classes for module", name)
                return Module(name, functions, classes), None

            if child.name == 'table':
                # We found a table
                assert not functions, "found two tables for module " + name
                functions = self.extract_functions(Function, child)
                print("Found", len(functions), "functions for module", name)
                continue

            elif child.name == 'a' and 'name' in child.attrs:
                # Beginning of a new class
                module_name, _, class_name = (child.get('name')
                                              .partition('.'))
                assert module_name == name
                continue

            else:
                # We didn't do anything with the element, so we just add it to
                # the description
                description += str(child)

    def to_api(self):
        # Collect these values
        modules = []
        # Indicator of next module name
        module_name = None

        self.children = iter(self.html.body.children)

        while True:
            if module_name:
                # Construct the module
                module, module_name = self.make_module(module_name)
                modules.append(module)
                print("created module", module.name)
                continue

            child = next(self.children, None)
            if not child:
                # Reached the end
                return SublimeApi(modules)

            elif child.name == 'p' and (child.a
                                        and child.a.get('name') == "sublime"):
                # The first anchor link is inside of an implicitly closed p tag,
                # which we need to fix manually
                child = child.a

            if child.name == 'a' and child.get('name'):
                # Each module/class is associated with an 'a' tag,
                # so search for it
                module_name = child.get('name')
                assert '.' not in module_name, module_name

    @staticmethod
    def from_path(path):
        with open(path, 'rt') as f:
            text = f.read()

        # Remove comments before parsing because they are a pain to handle
        text = re.sub(r'<!--.*?-->', '', text, flags=re.DOTALL)

        html = bs4.BeautifulSoup(text)
        return FromHtmlReader(html)


def read(path):
    """Parse API .html representation.

    @path
        Path to the .html representation of the Sublime Text API.
    """
    return FromHtmlReader.from_path(path)


if __name__ == '__main__':
    if os.path.exists('api.html'):
        api = read('api.html').to_api()
        print(api)
    else:
        here = os.path.abspath(os.path.dirname(__file__))
        print('no api.html file found in {}'.format(here))
